<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PagesController;
use App\Http\Middleware\SetLocale;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/vacation', function() {
    return redirect('https://goo.gl/forms/sa8TVBlYSdp1xwq73');
});

Route::group(['prefix' => '{locale}', 'where' => ['locale' =>  implode('|', config('app.locale_available'))], 'middleware' => 'setlocale'], function ($locale) {
Route::get('/', [PagesController::class, 'index'])->name('/');
Route::get('/vacancy/{vacancyName}', [PagesController::class, 'vacancy'])->where('vacancyName', 'web|ai|intern')->name('vacancy');
Route::get('/project/{projectWithCategory}', [PagesController::class, 'project'])->name('project');
});

Route::get('/', function () {
    return redirect(app()->getLocale());
});

