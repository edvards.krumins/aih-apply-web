server {
    listen       80;
    server_name 127.0.0.1 __SERVER_NAME_BACKEND__;
    
    root /var/www/html/public;
    client_max_body_size 32m;

    location ~ ^/(api|telescope|vendor/telescope)/ {
        add_header  X-IsBackend  1 always;
        # if this request is just for checking singledomain do not waste time returning response
        if ($http_IsBackend) {
            return 200;
        }

        # try to serve file directly, fallback to front controller
        # Simple requests
        try_files $uri /index.php$is_args$args;
    }

    # this allows single domain config (all routes not /api/*| will be proxied to frontend) for development
    location / {
        # tags responses so that js knows it is singledomain
        add_header  X-SingleDomain  1 always;
        # if this request is just for checking singledomain do not waste time returning response
        if ($http_IsSingleDomain) {
            return 200;
        }

        #static files
        root   /usr/share/nginx/html;
        index  index.html index.htm;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }
    }

    location ~ ^/index\.php(/|$) {
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Allow-Methods' 'GET,POST,PUT,PATCH,DELETE,OPTIONS' always;
        add_header 'Access-Control-Allow-Headers' 'Keep-Alive,User-Agent,X-Requested-With,Cache-Control,Content-Type,Authorization,HTTP_AUTHORIZATION' always;

        if ($request_method = OPTIONS ) {
          return 200;
        }

        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass applyweb-backend:9000;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }

    error_log  /dev/stderr;
    access_log /dev/stdout;
}
