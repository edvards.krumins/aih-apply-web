#!/bin/sh

CONF_PATH="/usr/share/nginx/html/config.js"
CONF_FOLDER="/usr/share/nginx/html"

mkdir -p ${CONF_FOLDER}
echo "window.injectedEnv = {" > "${CONF_PATH}"
printenv | grep VUE_ | sed -e "s/=\(.*\)/:\\\"\1\\\"/" | tr '\n' ',' >> "${CONF_PATH}"
echo "};" >> "${CONF_PATH}"

nginx -g "daemon off;"