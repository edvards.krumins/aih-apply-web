FROM nginx:1.17-alpine
LABEL maintainer="Andrejs Trabo <andrejs.trabo@applyit.lv>"

RUN apk --no-cache update \
  && rm -f /etc/nginx/conf.d/*.conf

WORKDIR /usr/share/nginx/html

COPY deployment/staging/entrypoint.nginx.sh /entrypoint.sh
COPY --from=__IMAGE__ /var/www/html .
COPY deployment/staging/nginx.conf /etc/nginx/nginx.conf
#COPY deployment/staging/nginx.applyweb-frontend.conf /etc/nginx/conf.d/applyweb-frontend.conf
COPY deployment/staging/nginx.applyweb-backend.conf /etc/nginx/conf.d/applyweb-backend.conf

# ENTRYPOINT ["nginx", "-g", "daemon off;"]
ENTRYPOINT ["/entrypoint.sh"]