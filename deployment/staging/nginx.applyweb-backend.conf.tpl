server {
    server_name 127.0.0.1 __SERVER_NAME_BACKEND__;
    
    client_max_body_size 128m;

    location / {
        root /usr/share/nginx/html/public;
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        root /var/www/html/public;
        add_header "Access-Control-Allow-Origin" * always;

        # Preflighted requests
        if ($request_method = OPTIONS) {
          add_header "Access-Control-Allow-Origin" * always;
          add_header "Access-Control-Allow-Methods" "GET,POST,PUT,PATCH,OPTIONS,HEAD,DELETE";
          add_header "Access-Control-Allow-Headers" "Authorization,Origin,X-Requested-With,Content-Type,Accept";
          return 200;
        }

        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass applyweb-backend:9000;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }

    error_log  /dev/stderr;
    access_log /dev/stdout;
}
