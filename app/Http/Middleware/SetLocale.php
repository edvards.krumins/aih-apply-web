<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\URL;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd("here");
        // dd("aaaaaa");
        // $locale = $request->segment(1) ?: App::getLocale();
        app()->setLocale($request->segment(1));
        // dd($locale);
        // dd("ok");
        // app()->setLocale($request->segment(1));
        // dd($request->segment(1));
        return $next($request);
        // if (starts_with($item->url, 'http')) {
        //     return;
        // }
        // if (LaravelLocalization::hideDefaultLocaleInURL() === false) {
        //     $item->url = locale() . '/' . preg_replace('%^/?' . locale() . '/%', '$1', $item->url);
        // }
    }
}
