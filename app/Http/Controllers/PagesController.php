<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PagesController extends Controller {

    public function index() {
        return view('pages.index', []);
    }

    public function vacancy($prefix, $vacancyName) {
        return view('pages.vacancy', compact('vacancyName'));
    }

    public function project($prefix, $projectWithCategory) {
        $category = explode('-', $projectWithCategory)[0];
        $projectName = explode('-', $projectWithCategory)[1];

        if (__("projects.$projectName") === "projects.$projectName") {
            abort(404);
        }
        
        return view('pages.project', compact('category', 'projectName'));
    }
}
