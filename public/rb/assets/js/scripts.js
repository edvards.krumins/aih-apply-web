// - - - - - - -  Prevent empty anchors to anchor  - - - - - - 
$(document).on('click', "a[href='#']", function(e) {
	e.preventDefault(e);
});


$( document ).ready(function() {

	// Animated scrollTo
$( ".navbar-nav a.nav-link, .scrolllink").click(function( event ) {
	$("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top + -50 }, 620);
	event.preventDefault();
});


});

function changeSwiperSpeed() {
	CaseStudySwiper.params.speed = 500;
}

const swiperConfig = {
	autoplay: {
		delay: 5000
	},
	speed: 1500,
	on: {
		autoplayStop: function () {
			changeSwiperSpeed();
		},
	},
	// Optional parameters
	//direction: 'vertical',
	//loop: true,

	// If we need pagination
	pagination: {
		el: '.swiper-pagination',
	},

	// Navigation arrows
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},

	// And if we need scrollbar
	scrollbar: {
		el: '.swiper-scrollbar',
	},
	breakpoints: {
	// when window width is >= 320px
	320: {
		slidesPerView: 1,
		spaceBetween: 30
	},
	// when window width is >= 480px
	480: {
		slidesPerView: 2,
		spaceBetween: 30
	},
	// when window width is >= 640px
	640: {
		slidesPerView: 3,
		spaceBetween: 30
	},
	centeredSlides: true
}
};

var fileTypeRe = /(?:\.([^.]+))?$/;
function drawSlide (video, image, title, description, page) {
	let locale = $('html').attr('lang');
	let media = null;
	if (video) {

		let type = fileTypeRe.exec(video)[1];
		
		if (image) {
			media = `<div class=\"img-ct thumb\" onclick="initVideo(this)">`
			+ `<img src=\"${image}\" data-video-src="/assets/media/video/${video}" data-video-type="video/${type}"></div>`;
		} else {
			media = `<div class=\"img-ct\"><video src="/assets/media/video/${video}" type="video/${type}" controls></video></div>`;
		}
	} else if (image) {
		media = `<div class=\"img-ct\"><img src=\"${image}\" alt=\"\"></div>`;
	}

	if (page) {
		return "<div class=\"swiper-slide case-study-item-sm\"><div class=\"thumbnail play\">"
							+ media
						+ "</div><div class=\"description\">"
						+ `<a href="${locale}/project/${page}" target="_blank"><h4>${title}</h4></a>`
					+ `<p>${description}</p>`
					+ `<a href="${locale}/project/${page}" target="_blank">Vairak par projektu >></a></div></div>`;
	} else {
		return "<div class=\"swiper-slide case-study-item-sm\"><div class=\"thumbnail play\">"
							+ media
						+ "</div><div class=\"description\">"
						+ `<h4>${title}</h4>`
					+ `<p>${description}</p></div></div>`;
	}

	
}

const slidesLib = {};

let CaseStudySwiper = null;

$(function() {
	CaseStudySwiper = new Swiper('.swiper-container.smedia', swiperConfig);

	selectSwiperCategory('media');

	$('.navbar-nav>li>a').on('click', function(){
		$('.navbar-collapse').collapse('hide');
	});
});

function selectSwiperCategory(category) {
	if (!slidesLib[category]) {
		slidesLib[category] = [];

		slidesData = document.getElementsByClassName("slide-" + category);

		let image = null;
		let title = null;
		let description = null;

		for (var slide of slidesData) {
			page = slide.getAttribute('data-page');
			video = slide.getAttribute('data-video');
			image = slide.getAttribute('data-image');
			title = slide.getAttribute('data-title');
			description = slide.getAttribute('data-description');

			slidesLib[category].push(drawSlide(video, image, title, description, page));
		}
	}

	let swiperWrapper = $('#case-studies-swiper-wrapper');
	
	if (slidesLib[category].length === 2) {
		swiperWrapper.addClass('center-content');
	} else {
		swiperWrapper.removeClass('center-content');
	}

	CaseStudySwiper.removeAllSlides();
	CaseStudySwiper.appendSlide(slidesLib[category]);
}

function initVideo (element) {
	const parent = $(element);
	parent.removeClass('thumb');

	const img = parent.children('img')[0];
	const source = img.getAttribute('data-video-src');
	const type = img.getAttribute('data-video-type');

	const video = $('<video />', {
	src: source,
	type: type,
	controls: true,
	autoplay: true
	});

	parent.append(video);
	parent.prop('onclick', null);
	
	img.remove();
}