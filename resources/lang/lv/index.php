<?php

return [
    'nav.home' => 'Sākums',
    'nav.case_studies' => 'MI projekti',
    'section.about_us.title' => 'Pakalpojumi',
    'nav.clients' => 'Mūsu klienti',
    'nav.vacancies' => 'Vakances',
    'nav.contacts' => 'Kontakti',
    'banner.title' => 'Jaudīgi un inovatīvi risinājumi',
    'banner.subtitle' => 'Izmantojot Mākslīgā Intelekta tehnoloģijas, jūs kļūstiet efektīvāki un konkurētspējīgāki',
    'banner.link.about_us' => 'Uzzini vairāk',
    'section.about_us.text.first' => 'Mākslīgā Intelekta sistēmu izstrāde jūsu efektīvam biznesam',
    'section.about_us.text.second' => 'IT sistēmu izstrāde sākot ar vienkāršiem risinājumiem, līdz biznesa kritiskām sistēmām un biznesa procesu automatizācijai',
    'section.about_us.text.third' => 'Tehnoloģiju pētījumi un to pielietojums, jaunu, inovatīvu produktu radīšanai',
    'section.case_studies.title' => 'Daži no realizētajiem projektiem',
    'section.case_studies.paragraph' => 'Ar Mākslīgā Intelekta (MI) risinājumiem mēs padarām dažādas industrijas daudz efektīvākas un konkurētspējīgākas. '
    . 'Pielietotās MI tehnoloģijas var paplašināt arī Jūsu biznesa potenciālu un nodrošināt tā transformāciju. ',
    'section.case_studies.media.title' => 'Mediji',
    'section.case_studies.media.adex.title' => 'AdEx – TV reklāmas monitorings',
    'section.case_studies.media.adex.descr' => 'Uz datorredzes tehnoloģijām balstīta sistēma, kas 7x24 analizē visus TV komerckanālus un nodrošina'
    . ' reklāmas piegādes statistiku. Platforma ļauj mēdiju tirgus analītiķiem un dažādām mēdiju un reklāmas aģentūrām efektīvāk un ātrāk plānot'
    . ' reklāmas kampaņas.',
    'section.case_studies.media.slash9.title' => 'Slash9 – TV arhīva attīrīšana',
    'section.case_studies.media.slash9.descr' => 'Uz datorredzes tehnoloģijām balstīta sistēma, kas paredzēta broadcasteriem un TV satura'
    . ' provaideriem, TV arhīva attīrīšanai – EPG precizēšana un satura atbrīvošana no reklāmām.',
    'section.case_studies.media.adspot.title' => 'Ad Spot Planner - Automātiska reklāmas izvietojuma plānošana TV kanālos',
    'section.case_studies.media.adspot.descr' => 'Sistēma palīdz saplānot TV broadkasteriem reklāmas kampaņas pa reklāmas pauzēm tā, lai sasniegtu'
    . ' pēc iespējas efektīvāk mērķauditoriju pēc iespējas īsākā laikā, izmantojot pēc iespējas mazāku apjomu no reklāmas inventorija.',
    'section.case_studies.industrial.title' => 'Industriālais',
    'section.case_studies.industrial.egg.title' => 'Olu uzskaite un klvalitātes kontrole',
    'section.case_studies.industrial.egg.descr' => 'Sistēma, izmantojot datorredzes tehnoloģijas, precīzi saskaita visas olas,'
    . ' kas slīd pa konveijeru līnijām no kūtīm, un saklasificē tās pēc bojājumiem, tīrības un citiem kritērijiem, un nodrošina'
    . ' produktivitātes un kvalitātes statistiku putnkopjiem turpmāku lēmumu pieņemšanai.',
    'section.case_studies.industrial.eggcrush.title' => 'Olu plēsējs - kvalitātes kontroles sistēma olu dzeltenuma un baltuma atdalīšanas procesā',
    'section.case_studies.industrial.eggcrush.descr' => 'Uz datorredzes balstīta kvalitātes kontroles sistēma novēro olu dzeltenuma un baltuma atdalīšanas procesu un tā'
    . ' rezultātu. Sistēma seko līdz kvalitātes kritērijiem un nodrošina reāla laika statistiku par iekārtas darbu un izejas'
    . ' produktu kvalitāti, ļaujot pieņemt lēmumus par iekārtas apkalpošanas nepieciešamību, kā arī sniedz produkta'
    . ' kvalitātes novērtējumu putnkopjiem turpmāku lēmumu pieņemšanai.',
    'section.case_studies.industrial.plastgran.title' => 'Plastmasas granulu kvalitātes kontrole',
    'section.case_studies.industrial.plastgran.descr' => 'Izmantojot datorredzi reālā laikā tiek veikta saražoto plastmasas granulu'
    . ' kvalitātes kontrole, novērtējot materiāla viendabīgumu, izmēru, krāsu uc. raksturlielumus, kas ļauj operatoram ātri'
    . ' pieņemt lēmumus par krāsns kalibrēšanas nepieciešamību vai izejas materiāla problēmām, un ļaujot novērst liekus'
    . ' saražotās produkcijas zudumus.',
    'section.case_studies.industrial.plastfilm.title' => 'Plastmasas plēves kvalitātes kontrole',
    'section.case_studies.industrial.plastfilm.descr' => 'Uz datorredzi balstīta kvalitātes kontroles sistēma novēro plēves ekstrudera saražotā'
    . ' produkta kvalitāti, reālā laikā uzskaitot plēvē atrastos defektus un nosakot saražotās plēves tekošo kvalitātes klasi.',
    'section.case_studies.industrial.metalcast.title' => 'Metāla liešanas smilšu veidņu kvalitātes kontroles sistēma',
    'section.case_studies.industrial.metalcast.descr' => 'Smilšu veidnei ir izšķiroša nozīme, metāla detaļu liešanā. Sistēma var identificēt veidnē'
    . ' ļoti mazus smilšu graudus kā arī veidnes nevienmērīgu, piemēram, veidnes izlūzumus un nodilumus. Sistēma var precīzi noteikt vietu, kur'
    . ' darbiniekam jāpievērš lielāka uzmanība.',
    'section.case_studies.industrial.glassbottle.title' => 'Stikla taras kvalitātes kontrole',
    'section.case_studies.industrial.glassbottle.descr' => 'Sadarbībā ar Valmiermuižas Alu un LEO KC notiek rūpnieciskais pētījums un'
    . ' eksperimentālā izstrāde, jauna tipa stikla taras kvalitātes kontroles sistēmas izveidei. Inovatīva datorredzes sistēma, kas būs'
    . ' uzstādāma uz esošas ražošanas līnijas un veiks stikla taras (pudeļu, glāžu, burciņu uc taras) kvalitātes kontroli. Sistēma spēs'
    . ' detektēt pat ar aci gandrīz neredzamus defektus. Konstatējot bojātu stikla taru sistēma nosūtīs signālu'
    . ' izpildmehānismam, ka attiecīgais objekts ir jānoņem no ražošanas līnijas. Sistēma būs adaptējama arī maziem pārtikas ražotājiem.',
    'section.case_studies.industrial.loundry.title' => 'Izmazgātās veļas kvalitātes kontroles sistēma',
    'section.case_studies.industrial.loundry.descr' => 'Izmantojot datorredzi sistēma novēro izmazgātās veļas locīšanas konveijera'
    . ' līniju un, konstatējot sabojātu/neizmazgātu palagu, nodrošina tā izņemšanu no gala produkcijas otrreizējai apstrādei.',
    'section.case_studies.industrial.autosort.title' => 'Robotizēta izkraušanas un šķirošanas iekārta',
    'section.case_studies.industrial.autosort.descr' => 'Sadarbībā ar PERUZA un ABB izstrādājām automatizētu sistēmu, kas spēj'
    . ' atpazīt un mijiedarboties ar haotiski novietotiem objektiem (random bin picking). Sistēma spēj atpazīst mīkstus un'
    . ' formu mainošus objektus (šajā gadījumā zivis), kas haotiski novietotas uz piem. ražošanas līnijas. Objektu koordinātas tiek'
    . ' pārsūtītas uz izpildes ierīci (robotu), kas attiecīgi atrod norādīto objektu, satvert un nogādā to vajadzīgajā vietā. Sistēma veic'
    . ' arī objekta mērījumus un atpazīst objekta orientāciju telpā.',
    'section.case_studies.smartcity.title' => 'Vied-pilsētas risinajumi',
    'section.case_studies.smartcity.autowatch.title' => 'Auto transporta plūsmas uzraudzība',
    'section.case_studies.smartcity.autowatch.descr' => 'Sadarbībā ar LMT esam izstrādājuši un piegādājuši auto transporta plūsmas uzraudzības'
    . ' sistēmu policijas vajadzībām Daugavpils rajonā. Sistēma 7/24 fiksē transportlīdzekļa kustības virzienu, notikuma laiku, reģ.nr., tipu u.c.'
    . ' vizuāli nosakāmus parametrus, un nodrošina informāciju policijas operatīvām un izmeklēšanas vajadzībām.',
    'section.case_studies.smartcity.truckcontrol.title' => 'Kravas transportlīdzekļu uzraudzība un kontrole',
    'section.case_studies.smartcity.truckcontrol.descr' => 'Kravas transportlīdzekļu kustības novērošana un kontrole uzņēmuma teritorijā – sistēma'
    . ' nodrošina automātisku transporta līdzekļu uzskaiti (vilcēja laiks, reģ.nr., svars, piekabes reģ.nr., svars), tā  integrējas ar ERP, auto'
    . ' svariem un vārtu kontroles sistēmām, lai nodrošinātu   paziņojumus par datu nesakritību vai novērotām anomālijām, kā arī pilnas notikuma'
    . ' ķēdes piefiksēšanu.',
    'section.case_studies.smartcity.trafficstress.title' => 'Satiksmes noslodzes uzskaites sistēma',
    'section.case_studies.smartcity.trafficstress.descr' => 'Sadarbībā ar SIA Metrum Jūrmalas pilsētā nodrošinājām automātisku ceļu'
    . ' infrastruktūras noslodzes mērījumus, uzskaitot transporta līdzekļus, to pārvietošanās virzienu un klasificējot tos pēc tipiem.',
    'section.case_studies.smartcity.parkingmonitoring.title' => 'Autostāvvietu aizpildīšanas monitorēšanas sistēma',
    'section.case_studies.smartcity.parkingmonitoring.descr' => 'Izmantojot novērošanas kameras un datorredzes tehnoloģijas, sistēma nosaka kuras'
    . ' vietas novērojamā autostāvvietā ir vai nav aizpildītas.',

    'section.case_studies.smartcity.dushelp.title' => 'DUS palīgs',
    'section.case_studies.smartcity.dushelp.descr' => 'Izmantojot DUS novērošanas kameras sistēma nolasa a/m reģ.nr. zīmi, nosaka'
    . ' a/m krāsu un tipu, tad salīdzina ar datubāzi (piem. CSDD), un, ja informācija nesakrīt, liedz klientam'
    . ' norēķināties ar pēcapmaksu, tādējādi novēršot iespējamu degvielas zādzību.',

    'section.case_studies.smartcity.checkout.title' => 'Automātiskās kases',
    'section.case_studies.smartcity.checkout.descr' => 'Bistro un kafejnīcām paredzēta, uz datorredzi balstīta, pašapkalpošanās kases sistēma,'
    . ' kas lokalizē un atpazīt dažādu tipa traukus, identificē atsevišķus ēdienus, kā arī dažādu ēdienu kombinācijas.  Sistēma atpazīst pirkumu'
    . ' un sastāda rēķinu apmaksai.',

    'section.case_studies.smartcity.deposit.title' => 'Depozīt sistēma izlietotās taras savākšanai',
    'section.case_studies.smartcity.deposit.descr' => 'Depozīta iekārta var identificēt iepakojumu bez etiķetēm un pat ja'
    . ' iepakojums ir sabojāts. Izmantojot datorredzes tehnoloģijas, nav nepieciešamas īpašas zīmes uz'
    . ' iepakojuma vai salasāms svītrkods, lai identificētu iepakojumu.',

    'section.case_studies.casino.title' => 'Kazino',
    'section.case_studies.casino.gameresult.title' => 'Automātiska spēlēs gaitas un rezultātu uzskaite (VTS)',
    'section.case_studies.casino.gameresult.descr' => 'Pielāgoti risinājumi tiešsaistes azartspēlēm uz vietas, pamatojoties uz'
    . ' video straumēšanu un reāllaika spēlēšanas procesiem. Kāršu atpazīšana, to kombinācijas un spēles loģikas uzskaite.'
    . ' Automātiski nosaka kazino žetonu rezultātus spēles zonā un uzskaita to vērtību.',
    'section.case_studies.casino.gameanalytics.title' => 'Online kazino spēles statistikas uzskaite',
    'section.case_studies.casino.gameanalytics.descr' => 'Datorredzes sistēma, kas ... ?????',

    'section.case_studies.microbiology.title' => 'Mikroskopija',
    'section.case_studies.microbiology.micropollute.title' => 'Mikroorganismu piesārņojuma līmeņa noteikšana dažādos produktos',
    'section.case_studies.microbiology.micropollute.descr' => 'Automatizēts mikroskopijas sistēmas risinājums, kas ļauj optiski analizēt'
    . ' dažādu produktu paraugus un atklāt nevēlamas piesārņojuma šūnas un mikroorganismus. Sistēma spēj arī'
    . ' analizēt un klasificēt mikroorganismu pēc to morfoloģiskām īpašībām. Šī sistēma būtiski saīsina laboratoriju testu laikus,'
    . ' ļaujot laborantam koncentrēties tikai uz rezultātu interpretāciju.',
    'section.case_studies.microbiology.microchip.title' => 'Mikročipu kontaktu kvalitātes kontrole',
    'section.case_studies.microbiology.microchip.descr' => 'Sistēma, kas izmantojot datorredzi analīzē mikročipu kontaktus un' 
    . ' klasificē tos pēc defektu klasēm.',

    'section.case_studies.healthcare.title' => 'Veselības aprūpe',
    'section.case_studies.healthcare.cheeksup.title' => 'CheeksUP',
    'section.case_studies.healthcare.cheeksup.descr' => 'Balstoties uz datorredzes tehnoloģijām sistēma spēj atpazīt pacienta sejas mīmiku'
    . ' un fiksēt tās amplitūdu. Sistēma uzlabo fizioterapētu-logopēdu darbu ar pacientiem, ar logopēdiskām un sejas'
    . ' muskulatūras funkcionalitātes problēmām.',

    'section.case_studies.healthcare.stroke.title' => 'Insulta skrīninga sistēma',
    'section.case_studies.healthcare.stroke.descr' => 'Sadarbībā ar radiologiem K. Kupču un A.Veissu, kā arī ar Farmācijas KC notiek'
    . ' rūpnieciskais pētījums un eksperimentālā izstrāde, izšēmiskā smadzeņu insulta skrīninga sistēmas prototipa izveidei.',

    'section.case_studies.finance.title' => 'Finanses un analītika',
    'section.case_studies.finance.electricity.title' => 'AJ elektrības pārvaldnieks',
    'section.case_studies.finance.electricity.descr' => '??? ???',

    'section.case_studies.other.title' => 'Dažādi',
    'section.case_studies.other.plants.title' => 'Augu kultūras noteikšana, izmantojot satelīta attēlus',
    'section.case_studies.other.plants.descr' => 'Sadarbībā ar SIA Autentica esam izstrādājuši datorredzes sistēmu, kas izmantojot Sentinel'
    . ' satelītu attēlus nosaka, kāda ir augu kultūra uzdotajā lauksaimniecības reģionā.',

    'section.contacts.title' => 'Sazinies ar mums',
    'section.contacts.email' => 'Epasts',
    'section.contacts.phone' => 'Tālrunis',
    'section.contacts.address.title' => 'Adrese',
    'section.contacts.address.content' => 'Ieriķu iela 5, Rīga, Latvija',
    'section.contacts.facebook' => 'Facebook',

    'section.vacancies.web.title' => 'Web izstrādātāja vakance',
    'section.vacancies.web.descr' => 'Īss apraksts ... ',
    'section.vacancies.ai.title' => 'AI izstrādātāja vakance',
    'section.vacancies.ai.descr' => 'Īss apraksts ... ',
    'section.vacancies.intern.title' => 'Praktikantu vakance',
    'section.vacancies.intern.descr' => 'Īss apraksts ... ',
];
