<?php

return [
    'web' => '
    <p><b>No Tevis sagaidām:</b></p>
    <ul>
        <li>Vismaz 3 gadu pieredze JS izstrādē izmantojot kādu no ietvariem: Vue, React vai analogu ietvaru</li>
        <li>Vismaz 1.5 gadu pieredze PHP izstrādē izmantojot Laravel, Symphony vai analogu ietvaru</li>
        <li>Pieredzi darbā (izstrādājot) Linux operētājsistēmā, un tās komandrindas lietošanā</li>
        <li>Labas Latviešu valodas zināšanas</li>
    </ul>
    <p><b>Augsti novērtēsim:</b></p>
    <ul>
        <li>Erudītas personas ar labu izpratni modernajās WEB tehnoloģijās un to pielietojumā.</li>    
        <li>Zināšanas/pieredzi sekojošās tehnaloģijās: Kubernetes, Docker, Rabbitmq, Mongo, NodeJS, Go lang, Python, Gitlab CI/CD vai analogs</li>    
        <li>Pieredzi izstrādājot mobilās aplikācijas - vēlams izmantojot React Native vai analogu.</li>    
        <li>Pieredzi integrācijā ar risinājumiem citās tehnoloģijās (piemēram C++)</li>    
        <li>Pieredzi integrācijās ar maksājumu vai citām ārējām sistēmām/API</li>    
    </ul>
    <p>Sūti savu CV un motivācijas vēstuli uz <a href="mailto:info@applyit.lv">info@applyit.lv</a></p>',

    'ai' => '<p><b>No tevis sagaidām:</b></p>
    <ul>
        <li>Vismaz 3 gadu pieredzi zemāk minētajās tehnoloģijās.</li>
        <li>Programmēšanas valodas: C++, Python.</li>
        <li>Pieredzi darbā ar bibliotēkām: OpenCv, Matplotlib.</li>
        <li>Pieredzi darbā ar ietvariem un platformām: Qt, Tensorflow, Caffe, PyTorch, Keras.* Linux, Docker.* SQL zināšanas. </li>
        <li>Pieredzi darbā ar datorredzi un pieredze datorredzes pielietošanā reālās vides problēmu risināšanai.</li>
        <li>Izpratni par attēla analīzes pamatalgoritmiem.</li>
        <li>Pieredzi darbā ar Neironu tīkliem, izpratne par to darbību.</li>
        <li>Teicamas latviešu un angļu valodas zināšanas.</li>
        <li>Spēja strādāt gan komandā, gan patstāvīgi un plānot savu darbu.</li>
        <li>Spēju plānot paredzamo darba apjomu.</li>
        <li>Vēlme apgūt jaunas tehnoloģijas.</li>
        <li>Augstākā vai nepabeigta augstākā izglītība datorzinātņu jomā.</li>
    </ul>
    <p>Sūti savu CV un motivācijas vēstuli uz <a href="mailto:info@applyit.lv">info@applyit.lv</a></p>',

    'intern' => '<p>Vai Tev patīk programmēt un Tevi interesē datorredze un mākslīgā intelekta izstrāde? '
    . 'Piedāvājam prakses vietas! Praksē apgūsi C++, PYTHON programmēšanas valodas un datorredzes pamatus. '
    . 'Pēc prakses beigām, Tu vari kļūt par vienu no mums un iegūt stabili atalgotu darbu vienā no dinamiskākajiem '
    . 'mākslīgā intelekta izstrādes uzņēmumiem!</p>
    <p>Sūti savu CV un motivācijas vēstuli uz <a href="mailto:info@applyit.lv">info@applyit.lv</a></p>'
];
