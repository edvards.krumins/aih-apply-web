<?php

return [
    'stroke' => '<img src="https://www.applyit.lv/fonds_EU.jpg" alt="Projekta Nr. 1.2.1.1/18/A/005">
 
        <h3>Projekta Nr. 1.2.1.1/18/A/005</h3>
        <p>Pētniecības projekta nosaukums:
        “Natīvu kompjūtertomogrāfijas attēlu radioloģiska izvērtēšana pacientiem ar akūtu išēmisku insultu priekšējā smadzeņu cirkulācijā - tehnoloģijas izstrāde un integrācija attēlu izvērtēšanas procesā pielietojot neironu tīklu”</p>
        <p>SIA ACCESS AV un SIA APPLY īsteno pētniecības projektu Darbības programmas "Izaugsme un nodarbinātībā" 1.2.1. specifiskā atbalsta mērķa "Palielināt privātā sektora investīcijas P&amp;A" 1.2.1.1. pasākumā " Atbalsts produktu un tehnoloģiju izstrādei kompetences centru ietvaros"</p>
        <p>Projekta mērķis ir izpētīt un izstrādāt neironu tīklu tehnoloģijas iespējas akūta išēmiska cerebrāla insulta identificēšanā natīvos kompjūtertomogrāfijas attēlos.</p>
        <p>Projekta pirmajā ceturksnī (01.11.2020 – 31.01.2021) izveidots vizuālo medicīnisko datu (datortomogrāfijas attēlu) klasifikācijas apgabalu marķēšanas rīks, kas nodrošina iespēju iezīmēt gan akūtu, gan vēsturisku išēmisko insultu lokācijas, gan arī dažādus smadzeņu apgabalus kā, piemēram, “smadzeņu vēderiņi”, kalcenīti un dažādas citas anomālijas.</p>',
    'glassbottle' => '<img src="https://www.applyit.lv/fonds_EU.jpg" alt="Projekta Nr. 1.2.1.1/18/A/005">
        <h3>Projekta Nr. 1.2.1.1/18/A/006</h3>
        <p>Pētniecības projekta nosaukums: “Stikla taras kvalitātes kontrole”</p>
        <p>SIA APPLY kā vadošais partneris un SIA Valmiermuižas alus kā partneris īsteno pētniecības projektu Darbības programmas '
        . '„Izaugsme un nodarbinātība” 1.2.1. specifiskā atbalsta mērķa „Palielināt privātā sektora investīcijas P&A” 1.2.1.1. pasākuma '
        . '„Atbalsts jaunu produktu un tehnoloģiju izstrādei kompetences centru ietvaros” ceturtās projektu iesniegumu atlases kārtas ietvaros.</p>
        <p>Pētījuma mērķis ir padarīt ekonomiski pieejamākas tukšās stikla taras kvalitātes kontroles tehnoloģijas kā arī paplašināt kvalitātes '
        . 'kontroles iespējas situācijās, kad šobrīd tirgū pieejamās sistēmas to nespēj. Šī pētījuma ietvaros izvirzītais biznesa mērķis ir inovatīva sistēma, kas ir adaptējama plašam ražotāju lokam ar akcentu uz iespēju '
        . 'sistēmu apmācīt un pielāgot visdažādāko vizuālās analīzes darbu veikšanai:</p>
        <ul>
            <li>noteikt taras piepildītības līmeni;</li>
            <li>etiķetes kvalitātes kontroli - gan vizuālo, gan novietojumu;</li>
            <li>etiķetes atbilstību tās plānotajam saturam;</li>
            <li>stikla taras kvalitātes parametrus - plaisas, gaisa ieslēgumi sienās, skrāpējumi;</li>
            <li>skalošanas/mazgāšanas šķidrumu atlieku klātbūtni tarā;</li>
            <li>zīmola īpatnību kvalitāti - logo, nestandarta formas atbilstības utml.</li>
        </ul>
        <p>Projekta pirmajā ceturksnī (01.11.2020 – 31.12.2020) sākta iekārtas teorētiskā izpēte: izmantojot spoguļu sistēmu un dažādu spektru '
        . 'gaismas avotu mijiedarbība; atpazīšanas un klasifikācijas algoritmu izpēte kā arī datu kopu izveide neironu tīklu apmācībai.</p>',
];
