@extends('layouts.sub_pages', ['title' => __('index.section.vacancies.' . $vacancyName . '.title')])

@section('content')

<section class="content-section vacancies-section py-5 vacancies" id="vacancies">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>{{ __('index.section.vacancies.' . $vacancyName . '.title') }}</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				{!! __('vacancy.' . $vacancyName) !!}
			</div>
		</div>
	</div>
</section>

<hr>

<section class="content-section py-5" id="contacts">
	@include('partials.index_contact_us')
</section>
@stop