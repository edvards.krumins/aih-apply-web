@extends('layouts.sub_pages', ['title' => __("index.section.case_studies.$category.$projectName.title")])

@section('content')

<section class="content-section vacancies-section py-5 vacancies" id="vacancies">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>{{ __("index.section.case_studies.$category.$projectName.title") }}</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				{!! __('projects.' . $projectName) !!}
			</div>
		</div>
	</div>
</section>

<hr>

<section class="content-section py-5" id="contacts">
	@include('partials.index_contact_us')
</section>
@stop