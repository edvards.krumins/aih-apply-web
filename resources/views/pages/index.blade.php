@extends('layouts.home')

@section('content')
<section class="cover cover-dark" id="home">

	<div class="cover-video">
		<div class='vid'>
			<video autoplay loop muted>
				<source src="/assets/media/video/APPLY_WEB_INTO.mp4" type="video/mp4" muted="muted">
				Your browser does not support the video tag.
			</video>
		</div>
		<!-- <iframe frameborder="0" height="100%" width="100%" allowfullscreen style="height: calc(100vw/1.77);"
		src="https://youtube.com/embed/A_M8WBJMcM0?playlist=A_M8WBJMcM0&autoplay=1&controls=0&showinfo=0&autohide=1&loop=1&mute=1&start=55&end=268;rel=0"> -->
		</iframe>
	</div>

	<div class="container">
		<div class="row py-4 my-4">
			<div class="col-lg-8 my-5">
				<h1>{{ __('index.banner.title') }}</h1>
				<p class="mt-4 pt-1 mb-4">{{ __('index.banner.subtitle') }}</p>
				<div class="pt-3">
					<a href="#contacts" class="btn btn-lg px-4 btn-primary mr-3 scrolllink">{{ __('index.section.contacts.title') }}</a>
					<!-- <a href="#case-studies" class="btn btn-lg px-4 btn-outline-light scrolllink">{{ __('index.banner.link.about_us') }}</a> -->
				</div>
			</div>
		</div>
	</div>

</section>

<section class="content-section about-us py-5" id="about-us">
	<div class="container py-5">
		<div class="row">
			<div class="col-lg-12 text-center px-5 pb-5">
				<h2>{{ __('index.section.about_us.title') }}</h2>
			</div>
			<div class="col-sm">
				<div class="lg-icon">
					<div class="icon-ct">
						<img src="/rb/assets/img/about-icons/ai-2.svg" alt="">
					</div>
				</div>
				<div>
					<p>{{ __('index.section.about_us.text.first') }}</p>
				</div>
			</div>
			<div class="col-sm mid">
				<div class="lg-icon">
					<div class="icon-ct">
						<img src="/rb/assets/img/about-icons/deep-learning.svg" alt="">
					</div>
				</div>
				<div>
					<p>{{ __('index.section.about_us.text.second') }}</p>
				</div>
			</div>
			<div class="col-sm">
				<div class="lg-icon">
					<div class="icon-ct">
						<img src="/rb/assets/img/about-icons/solutions.svg" alt="">
					</div>
				</div>
				<div>
					<p>{{ __('index.section.about_us.text.third') }}</p>
				</div>
			</div>
		</div>
	</div>
</section>

<hr>



<section class="content-section case-studies py-5" id="case-studies">
	@include('partials.index_case_studies')
</section>


<hr>


<section class="content-section py-5 our-clients" id="our-clients">
	<div class="container-fluid my-5">
		<div class="row mb-5">
			<div class="col-lg-12 text-center">
				<h2>{{ __('index.nav.clients') }}</h2>
				<!-- <p class="subheading">Lorem ipsum ...</p> -->
			</div>
		</div>
		<div class="row justify-content-center logo-grid">
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/intrum.png)" title="Intrum"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/tet.png)" title="TET"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/lmt.png)" title="LMT"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/balticovo.png)" title="Balticovo"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/lido.png)" title="Lido"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/conelum.png)" title="Conelum"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/madara.png)" title="Madara Cosmetics"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/scandicast.png)" title="Scandicast"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/beeking.png)" title="Beeking"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/abb.png)" title="ABB"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/cleantech.png)" title="Cleantech"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/dizozols.png)" title="Dižozols"></a>
			</div>
			<!-- <div class="col logo-item">
				<a	class="logo-ct no-logo"
					title="Video Tracking Solutions"
					style="line-height: 1.5em; display: flex; align-items: center;">Video Tracking Solutions</a>
			</div> -->
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/lindo.png)" title="Lindo"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/peruza.png)" title="Peruza"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/commreactor.png)" title="Reactor"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/latbal.png)" title="Latvijas Balzāms"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/valm.png)" title="Valmiermuiža"></a>
			</div>
			<div class="col logo-item">
				<a class="logo-ct" style="background:url(/rb/assets/img/logos/bw/plugnplay.png)" title="Plug And Play"></a>
			</div>
		</div>
	</div>
</section>


<hr>



<section class="content-section vacancies-section py-5 vacancies" id="vacancies">
	@include('partials.index_vacancies')
</section>


<hr>

<section class="content-section py-5" id="contacts">
	@include('partials.index_contact_us')
</section>
@stop