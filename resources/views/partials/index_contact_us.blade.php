<div class="container">
    <div class="row mb-4">
        <div class="col-lg-12 text-center">
            <h2>{{ __('index.section.contacts.title') }}</h2>
        </div>
    </div>
    <div class="row justify-content-center">


        <div class="col-sm">
            <div class="row">
                <div class="col text-center">
                    <a class="contact-item" href="mailto:info@applyit.lv">
                        <span class="ico-ct">
                            <img src="/rb/assets/img/contacts-icons/mail.svg" alt="">
                        </span>
                        <h3>{{ __('index.section.contacts.email') }}</h3>
                        <p>info@applyit.lv</p>
                    </a>
                </div>
                <div class="col text-center">
                    <a class="contact-item" href="tel:+37126135853">
                        <span class="ico-ct">
                            <img src="/rb/assets/img/contacts-icons/call.svg" alt="">
                        </span>
                        <h3>{{ __('index.section.contacts.phone') }}</h3>
                        <p>+371 261 358 53</p>
                    </a>
                </div>
            </div>
        </div>


        <div class="col-sm">
            <div class="row">
                <div class="col text-center">
                    <a class="contact-item" href="https://goo.gl/maps/1Xa6mMr5p3XHmTNt9" target="_blank">
                        <span class="ico-ct">
                            <img src="/rb/assets/img/contacts-icons/location.svg" alt="">
                        </span>
                        <h3>{{ __('index.section.contacts.address.title') }}</h3>
                        <p>{{ __('index.section.contacts.address.content') }}</p>
                    </a>
                </div>
                <div class="col text-center">
                    <a class="contact-item" href="https://www.facebook.com/ApplyIT.lv/" target="_blank">
                        <span class="ico-ct">
                            <img src="/rb/assets/img/contacts-icons/fb.svg" alt="">
                        </span>
                        <h3>{{ __('index.section.contacts.facebook') }}</h3>
                        <p>@applyit</p>
                    </a>
                </div>
            </div>
        </div>



    </div>
    <div class="row">
        <div class="col text-center">
            <p class="subheading mb-0"><b>Rekvizīti</b></p>
            <p>Adrese: SIA APPLY Ieriķu iela 5, Vidzemes priekšpilsēta, Rīga, LV-1084<br>
                Reg.Nr.: 44103077477 | AS “Citadele banka” | SWIFT: PARXLV22 | Citadele, LV45PARX0017072600001</p>
        </div>
    </div>
</div>