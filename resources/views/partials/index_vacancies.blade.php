<div class="container my-5">
    <div class="row mb-4">
        <div class="col-lg-12 text-center">
            <h2>{{ __('index.nav.vacancies') }}</h2>
            <!-- <p class="subheading">We're growing rapidly, and our goals are ambitious, so we're hiring equally ambitious people who will help us take APPLY to the next level. If you're up for new challenges and want to work in a global, fast-growing company - join our team! </p> -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="thumbnail">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'web']) }}" target="_blank" >

                    <div class="img-ct">
                        <img src="/assets/img/Vakance_web_dev.png" alt="">
                    </div>
                </a>
            </div>
            <div class="description">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'web']) }}" target="_blank" >
                    <h4>{{ __('index.section.vacancies.web.title') }}</h4>
                </a>
                <!-- <p>{{ __('index.section.vacancies.web.descr') }}</p> -->
            </div>
        </div>

        <div class="col-lg-4">
            <div class="thumbnail">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'ai']) }}" target="_blank" >
                    <div class="img-ct">
                        <img src="/assets/img/Vakance_AI.png" alt="">
                    </div>
                </a>
            </div>
            <div class="description">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'ai']) }}" target="_blank" >
                    <h4>{{ __('index.section.vacancies.ai.title') }}</h4>
                </a>
                <!-- <p>{{ __('index.section.vacancies.ai.descr') }}</p> -->
            </div>
        </div>

        <div class="col-lg-4">
            <div class="thumbnail">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'intern']) }}" target="_blank" >
                    <div class="img-ct">
                        <img src="/assets/img/Vakance_students.png" alt="">
                    </div>
                </a>
            </div>
            <div class="description">
                <a href="{{ route('vacancy', ['locale' => app()->getLocale(), 'vacancyName' => 'intern']) }}" target="_blank" >
                    <h4>{{ __('index.section.vacancies.intern.title') }}</h4>
                </a>
                <!-- <p>{{ __('index.section.vacancies.intern.descr') }}</p> -->
            </div>
        </div>
    </div> 
</div>