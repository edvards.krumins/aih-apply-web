<div class="container my-5">

    <div class="row mb-3">
        <div class="col-lg-12 text-center">
            <h2>{{ __('index.section.case_studies.title') }}</h2>
            <p class="subheading">{{ __('index.section.case_studies.paragraph') }}</p>
        </div>
    </div>

    <div id="industries">
        <nav class="row justify-content-center" id="industy_tabs" role="tablist">
            <div class="col-sm">
                <div class="row">
                    <a href="#cat-1" data-toggle="tab" aria-selected="false" class="col icon-item active" onclick="selectSwiperCategory('media')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/media.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.media.title') }}</h4>
                    </a>
                    <a href="#cat-2" data-toggle="tab" aria-selected="false" class="col icon-item" onclick="selectSwiperCategory('industrial')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/industrial.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.industrial.title') }}</h4>
                    </a>
                    <a href="#cat-3" data-toggle="tab" aria-selected="false" class="col icon-item" onclick="selectSwiperCategory('smartcity')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/surveillance.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.smartcity.title') }}</h4>
                    </a>
                </div>
            </div>

            <div class="col-sm">
                <div class="row">
                    <a href="#cat-4" data-toggle="tab" aria-selected="false" class="col icon-item" onclick="selectSwiperCategory('casino')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/casino.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.casino.title') }}</h4>
                    </a>
                    <a href="#cat-5" data-toggle="tab" aria-selected="false" class="col icon-item" onclick="selectSwiperCategory('microbiology')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/microbiology.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.microbiology.title') }}</h4>
                    </a>
                    <a href="#cat-6" data-toggle="tab" aria-selected="false" class="col icon-item" onclick="selectSwiperCategory('healthcare')">
                        <div class="ico-ct">
                            <img src="/rb/assets/img/icons/healthcare.svg" alt="">
                        </div>
                        <h4>{{ __('index.section.case_studies.healthcare.title') }}</h4>
                    </a>
                </div>
            </div>
        </nav>

        <div class="row">
            <div class="col">
                <!-- Slider main container -->
                <div class="swiper-container smedia" id="project-swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper" id="case-studies-swiper-wrapper">
                        <!-- Slides -->
                    </div>
                    <!-- If we need pagination -->
                    <!-- <div class="swiper-pagination"></div> -->

                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="slide-library d-none">
    <?php

    $sections = [
        'media' => [
            'adex' => [
                'video' => 'industrial.adex.mp4',
                'image' => '/assets/img/adEx.png'
            ],
            'slash9' => ['image' => '/assets/img/media.slash9.png'],
            'adspot' => ['image' => '/assets/img/AD_spot_planner.jpeg']
        ],
        'industrial' => [
            'egg' => [
                'video' => 'industrial.egg.mp4',
                'image' => '/assets/img/olu_uzskaite.png'
            ],
            'eggcrush' => [
                'video' => 'industrial.eggcrush.mp4',
                'image' => '/assets/img/olu_pleeseejs.png'
            ],
            'plastgran' => [
                'video' => 'industrial.plastgran.mp4',
                'image' => '/assets/img/Plastmasas_granulu_QA.png'
            ],
            'plastfilm' => [
                'video' => 'industrial.plastfilm.mp4',
                'image' => '/assets/img/Plastmasas_pleeves_QA.jpg'
            ],
            'metalcast' => [
                'video' => 'industrial.metalcast.mp4',
                'image' => '/assets/img/Metaala_lieshana.jpg'
            ],
            'glassbottle' => [
                'video' => 'industrial.glassbottle.mp4',
                'image' => '/assets/img/stikla_tara.png',
                'page' => 'industrial-glassbottle'
            ],
            'loundry' => [
                'video' => 'industrial.loundry.mp4',
                'image' => '/assets/img/Izmazgaata_velja.jpg'
            ],
            'autosort' => [
                'video' => 'industrial.autosort.mp4',
                'image' => '/assets/img/robo_izkrausana.png'
            ]
        ],
        'smartcity' => [
            'autowatch' => ['image' => '/assets/img/Autotransporta_plusmas_uzraudzība.jpg'],
            'truckcontrol' => ['image' => '/assets/img/Kravas transportlīdzekļu_uzraudzība.jpeg'],
            'trafficstress' => ['image' => '/assets/img/auto_transporta_pluusma.png'],
            'parkingmonitoring' => ['image' => '/assets/img/Auto stāvietu aispildīšana.jpg'],
            'dushelp' => ['image' => '/assets/img/DUS_paliigs.png'],
            'deposit' => [
                'video' => 'smartcity.deposit.mp4',
                'image' => '/assets/img/depoziits.jpg'
            ],
            'checkout' => [
                'video' => 'smartcity.checkout.mp4',
                'image' => '/assets/img/autom_kase.jpg'
            ],
        ],
        'casino' => [
            'gameresult' => ['image' => '/assets/img/Kazino.png'],
            // 'gameanalytics' => []
        ],
        'microbiology' => [
            'micropollute' => [
                'video' => 'microbiology.micropollute.mp4',
                'image' => '/assets/img/mikroorganismu_piesaarnjojums.jpg'
            ],
            'microchip' => ['image' => '/assets/img/Mikrocipu_kontaktu_QA.png']
        ],
        'healthcare' => [
            'cheeksup' => [
                'video' => 'healthcare.cheeksup.mp4',
                'image' => '/assets/img/Cheeks_up.jpg'
            ],
            'stroke' => [
                'image' => '/assets/img/insulta_skreeninga_sist.png',
                'page' => 'healthcare-stroke'
            ]
        ],
        // 'finance' => [
        //     'electricity' => ['image' => rand(0, 74)]
        // ],
        // 'other' => [
        //     'plants' => ['image' => rand(0, 74)]
        // ],
    ];

    ?>

    <?php foreach ($sections as $key => $projects) : ?>
        <span data-category="<?= $key ?>">
            <?php foreach ($projects as $pKey => $pData) : ?>
                <div class="slide-<?= $key ?>" data-video="<?= isset($pData['video']) ? $pData['video'] : null ?>" data-image="<?= isset($pData['image']) ? $pData['image'] : null ?>" data-title="{{ __('index.section.case_studies.' . $key . '.' . $pKey . '.title') }}" data-description="{{ __('index.section.case_studies.' . $key . '.' . $pKey . '.descr') }}" data-page="<?= isset($pData['page']) ? $pData['page'] : null ?>"></div>
            <?php endforeach; ?>
        </span>
    <?php endforeach; ?>
</div>

<script>
    $("#industy_tabs a").on("click", function() {
        $('#industy_tabs').find('.active').removeClass('active');

        if ($(window).width() <= 576) {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#project-swiper").offset().top - $('#app-navbar').height() - 20
            }, 500);
        }
        
    });
</script>