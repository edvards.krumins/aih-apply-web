<!DOCTYPE html>
<!-- <html lang="en"> -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<title>Apply Intelligent Technologies</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">

	<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="/rb/assets/css/style.css">
	<script type="text/javascript" src="/rb/assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/rb/assets/js/bootstrap.bundle.min.js"></script>
	<link rel="icon" href="/rb/assets/img/favicon.svg" color="#990000">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N8Q68G');</script>
	<!-- End Google Tag Manager -->
</head>
<body data-spy="scroll" data-target="#app-nav" data-offset="160">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N8Q68G"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Linkedin Tag -->
	<script type="text/javascript">
	_linkedin_data_partner_id = "110742";
	</script><script type="text/javascript">
	(function(){var s = document.getElementsByTagName("script")[0];
	var b = document.createElement("script");
	b.type = "text/javascript";b.async = true;
	b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
	s.parentNode.insertBefore(b, s);})();
	</script>
	<noscript>
		<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=110742&fmt=gif" />
	</noscript>
	<!--End of Linkedin Tag -->
	<nav class="navbar navbar-expand-lg app_navbar app-navbar-light" id="app-navbar">
		<div class="container">
			<a class="navbar-brand" href="./">
				<img src="/rb/assets/img/apply-it-logo-basic.svg" alt="APPLY">
			</a>
			<a href="#" class="navbar-toggler navigation-toggle collapsed" data-toggle="collapse" data-target="#app-nav" 
				aria-controls="app-nav" aria-expanded="false" aria-label="Toggle navigation">
				<hr><hr><hr>
			</a>
			<div class="collapse navbar-collapse ml-auto" id="app-nav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="#home" class="nav-link">{{ __('index.nav.home') }}</a></li>
					<li class="nav-item"><a href="#about-us" class="nav-link">{{ __('index.section.about_us.title') }}</a></li>
					<li class="nav-item"><a href="#case-studies" class="nav-link">{{ __('index.nav.case_studies') }}</a></li>
					<li class="nav-item"><a href="#our-clients" class="nav-link">{{ __('index.nav.clients') }}</a></li>
					<li class="nav-item"><a href="#vacancies" class="nav-link">{{ __('index.nav.vacancies') }}</a></li>
					<li class="nav-item"><a href="#contacts" class="nav-link">{{ __('index.nav.contacts') }}</a></li>
				</ul>
			</div>
      	</div>
    </nav>
	<div id="app">
		@yield('content')
	</div>
	<script type="text/javascript" src="/rb/assets/js/scripts.js"></script>
</body>
</html>
