<!DOCTYPE html>
<!-- <html lang="en"> -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="UTF-8">
	<title>{{ $title }} | Apply Intelligent Technologies</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/rb/assets/css/style.css">
	<script type="text/javascript" src="/rb/assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/rb/assets/js/bootstrap.bundle.min.js"></script>
	<link rel="icon" href="/rb/assets/img/favicon.svg" color="#990000">

</head>
<body data-spy="scroll" data-target="#app-nav" data-offset="160">
	<nav class="navbar navbar-expand-lg app_navbar app-navbar-light" id="app-navbar">
		<div class="container">
			<a class="navbar-brand" href="/">
				<img src="/rb/assets/img/apply-it-logo-basic.svg" alt="APPLY">
			</a>
      	</div>
    </nav>
    @yield('content')
    <footer>
</footer>

</body>
</html>
