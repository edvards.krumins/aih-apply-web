@extends('layouts.sub_pages', ['title' => '404 | Applyit'])

@section('content')

<section>
	<div>
		<div class="row">
			<div class="col">
				<h1 class="text-center">{{ '404' }}</h1>
			</div>
		</div>
	</div>
</section>
<section class="content-section py-5" id="contacts">
	@include('partials.index_contact_us')
</section>
@stop